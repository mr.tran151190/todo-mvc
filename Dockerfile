# This defines our starting point
FROM node:12

RUN mkdir /usr/src/app

WORKDIR /usr/src/app

RUN npm install -g @angular/cli@9.0.6

RUN npm install

COPY . .
