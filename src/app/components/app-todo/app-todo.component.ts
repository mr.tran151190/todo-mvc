import { Component, OnInit } from '@angular/core';
import { TodoService } from "../../services";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { faSearch, faBell, faUser, faTachometerAlt } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-app-todo',
  templateUrl: './app-todo.component.html',
  styleUrls: ['./app-todo.component.scss']
})
export class AppTodoComponent implements OnInit {
  faSearch = faSearch;
  faBell = faBell;
  faUser = faUser;
  faTachometerAlt = faTachometerAlt;

  cSidebar = true;

  hasTodo$: Observable<boolean>;

  constructor(private todoService: TodoService) { }

  ngOnInit() {
    this.todoService.fetchFromLocalStorage();
    this.hasTodo$ = this.todoService.length$.pipe(map((length) => length > 0));
  }
}
