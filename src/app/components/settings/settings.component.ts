import { Component, OnInit } from '@angular/core';
import { UserService, NotificationService } from "../../services";
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  listUser: any

  constructor(private userService: UserService, private notificationService: NotificationService,) { }

  ngOnInit(): void {
    this.userService
      .list()
      .pipe(first())
      .subscribe(
        (data: any) => {
          console.log(data)
          this.listUser = data.users
          this.notificationService.showSuccess("List user success", 'Success');
        },
        (error: any) => {
          this.notificationService.showError(error, "Error");
        }
      );
  }

  checkAll() {
    if ($("#mytable #checkall").is(':checked')) {
      $("#mytable input[type=checkbox]").each(function () {
        $(this).prop("checked", true);
      });

    } else {
      $("#mytable input[type=checkbox]").each(function () {
        $(this).prop("checked", false);
      });
    }
  }
}
