import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as $ from "jquery";
import { Router } from "@angular/router";

import { environment } from "@environments/environment";
import { AuthenticationService, UserService, NotificationService } from "../../services";
import { first } from 'rxjs/operators';

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"],
})
export class SidebarComponent implements OnInit {
  @Input() cSidebar: boolean;
  @Output() changeToggle: EventEmitter<any> = new EventEmitter<any>();

  currentUser: any;
  currentUrl: any = environment.apiUrl;

  fileToUpload: File = null;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private notificationService: NotificationService,
  ) {
    this.authenticationService.currentUser.subscribe(
      (x) => (this.currentUser = x)
    );
  }

  ngOnInit(): void {
    $(".sidebar-dropdown > a").click(function () {
      $(".sidebar-submenu").slideUp(200);
      if ($(this).parent().hasClass("active")) {
        $(".sidebar-dropdown").removeClass("active");
        $(this).parent().removeClass("active");
      } else {
        $(".sidebar-dropdown").removeClass("active");
        $(this).next(".sidebar-submenu").slideDown(200);
        $(this).parent().addClass("active");
      }
    });

    $(".sidebar-submenu > ul > li > a").click(function () {
      if ($(this).parent().hasClass("active-item")) {
        $(".sidebar-item").removeClass("active-item");
        $(this).parent().removeClass("active-item");
      } else {
        $(".sidebar-item").removeClass("active-item");
        $(this).parent().addClass("active-item");
      }
    });
  }

  toogleSidebar() {
    this.cSidebar = !this.cSidebar;
    this.changeToggle.emit(this.cSidebar);
  }

  uploadAvatar(file: any) {
    const userId = this.currentUser.user._id.$oid;
    this.userService
      .uploadAvatarUser(userId, file)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.notificationService.showSuccess(data.message, 'Success');
          let user = JSON.parse(localStorage.getItem("currentUser"))
          user.user.avatarUrl = data.avatarUrl
          this.currentUser = user
          localStorage.setItem("currentUser", JSON.stringify(user));
        },
        (error: any) => {
          this.notificationService.showError(error, "Error");
        }
      );
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(["/login"]);
  }
}
