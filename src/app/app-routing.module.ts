import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from "./components/register/register.component";
import { LoginComponent } from "./components/login/login.component";
import { HomeComponent } from "./components/home/home.component";
import { AppTodoComponent } from "./components/app-todo/app-todo.component";
import { SettingsComponent } from "./components/settings/settings.component";
import { AuthGuard } from './helpers';

const routes: Routes = [
  // register
  { path: "register", component: RegisterComponent },
  // login
  { path: "login", component: LoginComponent },

  // Trang chu
  { path: "", component: HomeComponent, canActivate: [AuthGuard] },
  { path: "todo-list", component: AppTodoComponent, canActivate: [AuthGuard] },
  { path: "setting-members", component: SettingsComponent, canActivate: [AuthGuard] },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
