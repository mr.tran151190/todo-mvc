import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { TodoListComponent } from "./components/app-todo/todo-list/todo-list.component";
import { TodoItemComponent } from "./components/app-todo/todo-item/todo-item.component";
import { TodoInputComponent } from "./components/app-todo/todo-input/todo-input.component";
import { HeaderComponent } from "./components/app-todo/header/header.component";
import { FooterComponent } from "./components/app-todo/footer/footer.component";
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AppTodoComponent } from './components/app-todo/app-todo.component';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';

import { JwtInterceptor, ErrorInterceptor } from "./helpers";
import { ToastrModule } from "ngx-toastr";
import { AlertComponent } from './components/elements/alert/alert.component';
import { SettingsComponent } from './components/settings/settings.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    TodoListComponent,
    TodoItemComponent,
    TodoInputComponent,
    HeaderComponent,
    FooterComponent,
    AppTodoComponent,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    AlertComponent,
    SettingsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    ToastrModule.forRoot()
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
