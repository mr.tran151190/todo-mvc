import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';

import { AuthenticationService } from './services';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  cSidebar = true;
  currentUser: any;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {

  }

  toogleSidebar() {
    this.cSidebar = !this.cSidebar;
  }

  onToogleSidebar(value: any) {
    this.cSidebar = value
  }
}
