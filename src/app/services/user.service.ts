import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { environment } from "@environments/environment";

@Injectable({ providedIn: "root" })
export class UserService {
  headers: any;
  constructor(private http: HttpClient) { }

  register(user: object) {
    return this.http.post(`${environment.apiUrl}/register`, user);
  }

  list() {
    return this.http.get(`${environment.apiUrl}/ListUser`);
  }

  uploadAvatarUser(userId: string, file: any) {
    let formData = new FormData();
    formData.append("formData", file);
    return this.http.put(`${environment.apiUrl}/UploadAvatarUser/${userId}`, formData, { headers: this.headers });
  }
}
