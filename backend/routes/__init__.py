from extensions import api
from modules.accounts.AccountsResource import AccountsView, AccountView
from modules.auth.AuthResource import LoginView, RegisterView
from modules.users.User import UploadAvatarUserView, ListUserView
