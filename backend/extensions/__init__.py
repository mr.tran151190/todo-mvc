from flask import Blueprint
from flask_jwt_extended import JWTManager
from flask_mongoengine import MongoEngine
from flask_restful import Api

from exceptions import errors, modify_error_response
from .JSONExceptionHandler import JSONExceptionHandler

bp = Blueprint('api', __name__, static_folder='static')

db = MongoEngine()
api = Api(bp, errors=errors, catch_all_404s=True)

jwt = JWTManager()

exception_handler = JSONExceptionHandler()

def init_extensions(app):
    """
    Application extensions initialization.
    """
    for extension in (
            db,
            api,
            jwt,
            exception_handler
    ):
        extension.init_app(app)

    modify_error_response(jwt, exception_handler)
