# Manual

## Installation

Note: py or python

- Tạo virtual enviroment

```buildoutcfg
py -m venv venv
or
python3 -m venv venv
```

- Activate venv

```buildoutcfg
source venv/bin/activate
```

- Cài đặt các packages

```buildoutcfg
pip install -r requirements.txt
```

- Tạo seed

```buildoutcfg
python manage.py seed --file [PATH]
vd:
python manage.py seed --file seeder/accounts.json
```

- Run app

```buildoutcfg
python app.py
```

- UnitTest

```buildoutcfg
python -m unittest tests/TestApp.py
```

## Swagger

Truy cập swagger qua url:

```buildoutcfg
http://localhost:5000/ui/
```

Mô tả cho Swagger được viết trong swagger/swagger.yaml
