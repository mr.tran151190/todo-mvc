import connexion
from connexion.resolver import MethodViewResolver

from flask_cors import CORS
from flask_jwt_extended import JWTManager

from extensions import bp, init_extensions


def create_app():
    app = connexion.FlaskApp(__name__, port=5000, specification_dir='swagger/')
    app.add_api('swagger.yaml',
                arguments={'title': 'MethodViewResolver Example'},
                resolver=MethodViewResolver('routes'))
    app.app.config.from_object('config.DevelopmentConfig')
    app.app.secret_key = 'super secret key'

    init_extensions(app.app)

    return app


app = create_app()

CORS(app.app)

jwt = JWTManager(app.app)

if __name__ == '__main__':
    app.run(debug=True)
