import unittest

from app import app
from extensions import db


class BaseCase(unittest.TestCase):

    def setUp(self):
        self.app = app.app.test_client()

        self.db = db.get_db()

    def tearDown(self):
        # Drop all collections
        # for collection in self.db.list_collection_names():
        #     self.db.drop_collection(collection)
        pass
