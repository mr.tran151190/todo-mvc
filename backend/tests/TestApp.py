import json

from tests import BaseCase


class TestApp(BaseCase):

    def get_access_token(self):
        # Given
        username = "test"
        password = "111111"
        user_payload = json.dumps({
            "username": username,
            "password": password
        })

        response = self.app.post(
            '/login', headers={"Content-Type": "application/json"}, data=user_payload)
        return response.json['access_token']

    def test_a01_register_successful(self):
        self.db.drop_collection('users')

        # Given
        payload = json.dumps({
            "username": "test",
            "password": "111111",
            "email": "test11@test.com",
            "role": 1
        })

        # When
        response = self.app.post(
            '/register', headers={"Content-Type": "application/json"}, data=payload)

        # Then
        self.assertEqual(str, type(response.json['id']))
        self.assertEqual(200, response.status_code)

    def test_a02_register_fail(self):
        # Given
        payload = json.dumps({
            "username": "",
            "password": "111111",
            "email": "test11@test.com",
            "role": 1
        })

        # When
        response = self.app.post(
            '/register', headers={"Content-Type": "application/json"}, data=payload)

        # Then
        self.assertEqual(str, type(response.json['message']))
        self.assertEqual(400, response.status_code)

    def test_b01_login_successful(self):
        # Given
        username = "test"
        password = "111111"
        payload = json.dumps({
            "username": username,
            "password": password
        })

        response = self.app.post(
            '/login', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual(200, response.status_code)
        self.assertEqual(list, type(response.json['access_token']))

    def test_b02_login_fail(self):
        # Given
        username = "test"
        password = "11111111"
        payload = json.dumps({
            "username": username,
            "password": password
        })

        response = self.app.post(
            '/login', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual(400, response.status_code)

    def test_c01_create_account_successfully(self):
        access_token = self.get_access_token()

        account_payload = {
            "account_number": 1234,
            "balance": 500,
            "firstname": "tuyen1",
            "lastname": "tuyen1",
            "age": 36,
            "gender": "M",
            "address": "671 Bristol Street",
            "employer": "Tuyen",
            "email": "tuyem11d1d221asdx@rk.com",
            "city": "Dante",
            "state": "TN"
        }

        # When
        response = self.app.post('/accounts', headers={"Content-Type": "application/json",
                                                           "Authorization": f"Bearer {access_token[0]}"},
                                 data=json.dumps(account_payload))

        self.assertEqual('Created account successful', response.json['message'])
        self.assertEqual(200, response.status_code)
    
    def test_c01_create_account_error_required(self):
        access_token = self.get_access_token()

        account_payload = {
            "account_number": 12345,
            "balance": 500,
            "firstname": "tuyen1",
            "lastname": "",
            "age": 36,
            "gender": "M",
            "address": "671 Bristol Street",
            "employer": "Tuyen",
            "email": "tuyem11d1d221asdx@rk.com",
            "city": "Dante",
            "state": "TN"
        }

        # When
        response = self.app.post('/accounts', headers={"Content-Type": "application/json",
                                                       "Authorization": f"Bearer {access_token[0]}"},
                                 data=json.dumps(account_payload))

        self.assertEqual('error', response.json['status'])
        self.assertEqual(201, response.status_code)

    def test_c01_create_account_invalid_email(self):
        access_token = self.get_access_token()

        account_payload = {
            "account_number": 123456,
            "balance": 500,
            "firstname": "tuyen1",
            "lastname": "",
            "age": 36,
            "gender": "M",
            "address": "671 Bristol Street",
            "employer": "Tuyen",
            "email": "tuyemcom",
            "city": "Dante",
            "state": "TN"
        }

        # When
        response = self.app.post('/accounts', headers={"Content-Type": "application/json",
                                                       "Authorization": f"Bearer {access_token[0]}"},
                                 data=json.dumps(account_payload))

        self.assertEqual('error', response.json['status'])
        self.assertEqual(201, response.status_code)

    def test_c02_create_account_failed_exists(self):
        access_token = self.get_access_token()

        account_payload = {
            "account_number": 1234,
            "balance": 500,
            "firstname": "tuyen1",
            "lastname": "tuyen1",
            "age": 36,
            "gender": "M",
            "address": "671 Bristol Street",
            "employer": "Tuyen",
            "email": "tuyem11d1d221x@rk.com",
            "city": "Dante",
            "state": "TN"
        }

        # When
        response = self.app.post('/accounts', headers={"Content-Type": "application/json",
                                                           "Authorization": f"Bearer {access_token[0]}"},
                                 data=json.dumps(account_payload))

        self.assertEqual(
            'error', response.json['status'])
        self.assertEqual(201, response.status_code)

    def test_d01_get_account_successfully(self):
        access_token = self.get_access_token()

        # When
        response = self.app.get('/accounts', headers={"Authorization": f"Bearer {access_token[0]}"})

        self.assertEqual('success', response.json['status'])
        self.assertEqual(200, response.status_code)

    def test_e01_update_account_successfully(self):
        access_token = self.get_access_token()

        account_payload = {
            "account_number": 1234,
            "balance": 50000,
            "firstname": "tuyen1",
            "lastname": "tuyen1",
            "age": 36,
            "gender": "M",
            "address": "671 Bristol Street",
            "employer": "Tuyen",
            "email": "adasd@rk.com",
            "city": "Dante",
            "state": "TN"
        }

        # When
        response = self.app.put('/account/1234', headers={"Content-Type": "application/json",
                                                               "Authorization": f"Bearer {access_token[0]}"},
                                data=json.dumps(account_payload))

        self.assertEqual('Update successful', response.json['message'])
        self.assertEqual(200, response.status_code)

    def test_e01_update_account_error_required_firstname(self):
        access_token = self.get_access_token()

        account_payload = {
            "account_number": 1234,
            "balance": 50000,
            "firstname": "",
            "lastname": "123",
            "age": 36,
            "gender": "M",
            "address": "671 Bristol Street",
            "employer": "Tuyen",
            "email": "adasd@rk.com",
            "city": "Dante",
            "state": "TN"
        }

        # When
        response = self.app.put('/account/1234', headers={"Content-Type": "application/json",
                                                          "Authorization": f"Bearer {access_token[0]}"},
                                data=json.dumps(account_payload))

        self.assertEqual(
            'error', response.json['status'])
        self.assertEqual(201, response.status_code)

    def test_e01_update_account_error_required_lastname(self):
        access_token = self.get_access_token()

        account_payload = {
            "account_number": 1234,
            "balance": 50000,
            "firstname": "123",
            "lastname": "",
            "age": 36,
            "gender": "M",
            "address": "671 Bristol Street",
            "employer": "Tuyen",
            "email": "adasd@rk.com",
            "city": "Dante",
            "state": "TN"
        }

        # When
        response = self.app.put('/account/1234', headers={"Content-Type": "application/json",
                                                          "Authorization": f"Bearer {access_token[0]}"},
                                data=json.dumps(account_payload))

        self.assertEqual(
            'error', response.json['status'])
        self.assertEqual(201, response.status_code)
    
    def test_e01_update_account_error_required_email(self):
        access_token = self.get_access_token()

        account_payload = {
            "account_number": 1234,
            "balance": 50000,
            "firstname": "123",
            "lastname": "123",
            "age": 36,
            "gender": "M",
            "address": "671 Bristol Street",
            "employer": "Tuyen",
            "email": "",
            "city": "Dante",
            "state": "TN"
        }

        # When
        response = self.app.put('/account/1234', headers={"Content-Type": "application/json",
                                                          "Authorization": f"Bearer {access_token[0]}"},
                                data=json.dumps(account_payload))

        self.assertEqual(
            'error', response.json['status'])
        self.assertEqual(201, response.status_code)

    def test_e01_update_account_error_invalid_email(self):
        access_token = self.get_access_token()

        account_payload = {
            "account_number": 1234,
            "balance": 50000,
            "firstname": "123",
            "lastname": "123",
            "age": 36,
            "gender": "M",
            "address": "671 Bristol Street",
            "employer": "Tuyen",
            "email": "tuyen",
            "city": "Dante",
            "state": "TN"
        }

        # When
        response = self.app.put('/account/1234', headers={"Content-Type": "application/json",
                                                          "Authorization": f"Bearer {access_token[0]}"},
                                data=json.dumps(account_payload))

        self.assertEqual(
            'error', response.json['status'])
        self.assertEqual(201, response.status_code)

    def test_e01_update_account_failed_exists(self):
        access_token = self.get_access_token()

        account_payload = {
            "account_number": 1234,
            "balance": 50000,
            "firstname": "123",
            "lastname": "321",
            "age": 36,
            "gender": "M",
            "address": "671 Bristol Street",
            "employer": "Tuyen",
            "email": "connieberry@flumbo.com",
            "city": "Dante",
            "state": "TN"
        }

        # When
        response = self.app.put('/account/1234', headers={"Content-Type": "application/json",
                                                          "Authorization": f"Bearer {access_token[0]}"},
                                data=json.dumps(account_payload))

        self.assertEqual(
            'error', response.json['status'])
        self.assertEqual(201, response.status_code)

    def test_f01_delete_account_successfully(self):
        access_token = self.get_access_token()

        # When
        response = self.app.delete('/account/1234',
                                   headers={"Content-Type": "application/json",
                                            "Authorization": f"Bearer {access_token[0]}"})

        self.assertEqual('Deleted successful', response.json['message'])
        self.assertEqual(200, response.status_code)

    def test_f02_delete_not_exist_account(self):
        access_token = self.get_access_token()

        # When
        response = self.app.delete('/account/12345',
                                   headers={"Content-Type": "application/json",
                                            "Authorization": f"Bearer {access_token[0]}"})

        self.assertEqual(400, response.status_code)
