# ~exercise-python-angular/server/resources/errors.py
from flask import jsonify

errors = {
    "SchemaValidationError": {
        "message": "Request is missing required fields",
        "status": 400
    },
    "AccountNotExistsError": {
        "message": "Account with given id doesn't exists",
        "status": 400
    },
    "EmailAlreadyExistsError": {
        "message": "User with given email or username address already exists",
        "status": 400
    },
    "UnauthorizedError": {
        "message": "Invalid username or password",
        "status": 400
    },
    "UserAuthorizedError": {
        "message": "You are not authorized",
        "status": 401
    },
    "AccountAlreadyExistsError": {
        "message": "Account with given email or account number already exists",
        "status": 400
    },
    "UpdatingAccountError": {
        "message": "Updating account added by other is forbidden",
        "status": 403
    },
    "DeletingAccountError": {
        "message": "Deleting account added by other is forbidden",
        "status": 403
    },
    "NotFound": {
        "message": "Not found.",
        "status": 404
    },
    "InternalServerError": {
        "message": "Something went wrong",
        "status": 500
    },
}


def modify_error_response(jwt, exception_handler):
    for e in (
            InternalServerError, SchemaValidationError, AccountAlreadyExistsError, UpdatingAccountError,
            DeletingAccountError,
            AccountNotExistsError, EmailAlreadyExistsError, UnauthorizedError, UserAuthorizedError):
        exception_handler.register(e)

    @jwt.expired_token_loader
    def my_expired_token_callback(expired_token):
        token_type = expired_token['type']
        return jsonify({
            'status': 401,
            'msg': 'The {} token has expired.'.format(token_type)
        }), 401

    @jwt.unauthorized_loader
    def unauthorized_loader_callback(callback):
        return jsonify({
            'status': 401,
            'msg': 'Authorization header is missing.'.format()
        }), 401

    @jwt.claims_verification_loader
    def claims_verification_loader_callback(callback):
        return callback

    @jwt.claims_verification_failed_loader
    def claims_verification_failed_loader_callback():
        return jsonify({
            'status': 400,
            'msg': 'User claims verification failed.'.format()
        }), 400


class BaseException(Exception):
    def __init__(self):
        self.status = errors.get(self.__class__.__name__).get("status")
        self.message = errors.get(self.__class__.__name__).get("message")


class InternalServerError(BaseException):
    pass


class SchemaValidationError(BaseException):
    pass


class AccountAlreadyExistsError(BaseException):
    pass


class UpdatingAccountError(BaseException):
    pass


class DeletingAccountError(BaseException):
    pass


class AccountNotExistsError(BaseException):
    pass


class EmailAlreadyExistsError(BaseException):
    pass


class UnauthorizedError(BaseException):
    pass


class UserAuthorizedError(BaseException):
    pass
