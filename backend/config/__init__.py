class Config(object):
    JWT_ACCESS_COOKIE_PATH = "/api/"
    JWT_REFRESH_COOKIE_PATH = "/token/refresh"
    JWT_COOKIE_CSRF_PROTECT = True
    JWT_SECRET_KEY = "t1NP63m4wnBg6nyHYKfmc2TpCOGI4nss"

    MONGODB_SETTINGS = {
        # 'host': "mongodb://localhost:27017/anm_viettel",
        'db': 'anm_viettel',
        'host': 'db',
        'port': 27017
    }


class ProductionConfig(Config):
    ENV = "production"
    FLASK_ENV = "production"
    FLASK_DEBUG = 0
    # SQLALCHEMY_DATABASE_URI = "mongodb://localhost:27017/anm_viettel"
    # SQLALCHEMY_TRACK_MODIFICATIONS = True


class DevelopmentConfig(Config):
    ENV = "development"
    FLASK_ENV = "development"
    FLASK_DEBUG = 1


class TestingConfig(Config):
    TESTING = True
    ENV = "development"
    FLASK_ENV = "development"
    FLASK_DEBUG = 1
