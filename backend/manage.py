from flask import Flask
from flask_mongoengine import MongoEngine
from flask_script import Manager

from seeder.run import Seed

app = Flask(__name__)
app.config.from_object('config.DevelopmentConfig')

db = MongoEngine(app)

manager = Manager(app)
manager.add_command('seed', Seed(app=app, db=db))

if __name__ == "__main__":
    manager.run()
