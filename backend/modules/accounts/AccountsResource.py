# ~anm-viettel-backend/modules/accounts/AccountsResource.py

import re

from flask import request
from flask.views import MethodView
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource, reqparse
from mongoengine.errors import FieldDoesNotExist, NotUniqueError, DoesNotExist, ValidationError, InvalidQueryError

from exceptions import SchemaValidationError, AccountAlreadyExistsError, InternalServerError, \
    UpdatingAccountError, UserAuthorizedError, AccountNotExistsError
from modules.accounts.AccountModel import Accounts

parser = reqparse.RequestParser()

parser.add_argument('page')
parser.add_argument('items_per_page')
parser.add_argument('search')
parser.add_argument('sortbyName')
parser.add_argument('sortbyValue')
parser.add_argument('order')
parser.add_argument('account_number')
parser.add_argument('balance')
parser.add_argument('firstname')
parser.add_argument('lastname')
parser.add_argument('age')
parser.add_argument('gender')
parser.add_argument('address')
parser.add_argument('employer')
parser.add_argument('email')
parser.add_argument('city')
parser.add_argument('state')

# Make a regular expression
# for validating an Email
regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'


class AccountsView(MethodView):
    @jwt_required
    def search(self, page=1, items_per_page=30, search=None):
        try:
            args = parser.parse_args()
            items_per_page = int(
                args.items_per_page) if args.items_per_page is not None else items_per_page
            page = int(args.page) if args.page is not None else page

            limit = page * items_per_page
            skip = limit - items_per_page

            filters = {}

            if args.account_number is not None:
                filters['account_number'] = int(args.account_number)
            if args.balance is not None:
                filters['balance'] = int(args.balance)
            if args.firstname is not None:
                filters['firstname'] = {'$regex': args.firstname.strip()}
            if args.lastname is not None:
                filters['lastname'] = {'$regex': args.lastname.strip()}
            if args.age is not None:
                filters['age'] = int(args.age)
            if args.gender is not None:
                filters['gender'] = args.gender
            if args.address is not None:
                filters['address'] = {'$regex': args.address.strip()}
            if args.employer is not None:
                filters['employer'] = {'$regex': args.employer.strip()}
            if args.email is not None:
                filters['email'] = {'$regex': args.email.strip()}
            if args.city is not None:
                filters['city'] = {'$regex': args.city.strip()}
            if args.state is not None:
                filters['state'] = {'$regex': args.state.strip()}

            accounts = Accounts.objects[
                       skip: limit
                       ](**filters)
            total = accounts.count()
            '''
            sortbyName=balance,-age....
            '''

            if args.sortbyName is not None:
                sortsName = [x.strip() for x in args.sortbyName.split(',')]
                accounts = accounts.order_by(*sortsName, args.sortbyValue)

            return {'data': {'accounts': accounts.to_json(), 'total': total}, 'status': 'success'}, 200
        except Exception:
            raise InternalServerError

    @jwt_required
    def post(self):
        user = get_jwt_identity()
        if user['role'] == 1:
            try:
                body = request.get_json()

                # required Input
                if body['account_number'] is None:
                    return {'message': 'Account number do not empty!', 'status': 'error'}, 201
                if body['balance'] is None:
                    return {'message': 'Balance do not empty!', 'status': 'error'}, 201
                if not body['firstname']:
                    return {'message': 'Firstname do not empty!', 'status': 'error'}, 201
                if not body['lastname']:
                    return {'message': 'Lastname do not empty!', 'status': 'error'}, 201
                if body['age'] is None:
                    return {'message': 'Age do not empty!', 'status': 'error'}, 201
                if body['age'] == 0:
                    return {'message': 'Age > 0!', 'status': 'error'}, 201
                if not body['gender']:
                    return {'message': 'Gender do not empty!', 'status': 'error'}, 201
                if not body['address']:
                    return {'message': 'Address do not empty!', 'status': 'error'}, 201
                if not body['email']:
                    return {'message': 'Email do not empty!', 'status': 'error'}, 201
                if not body['city']:
                    return {'message': 'City do not empty!', 'status': 'error'}, 201
                if not body['state']:
                    return {'message': 'State do not empty!', 'status': 'error'}, 201
                if not body['employer']:
                    return {'message': 'Employer do not empty!', 'status': 'error'}, 201

                # check format email
                if not (re.search(regex, body['email'])):
                    return {'message': 'Invalid Email!', 'status': 'error'}, 201

                # check exist account number
                account = Accounts.objects(account_number=body['account_number'])

                if account:
                    return {'message': 'Duplicate account number', 'status': 'error'}, 201

                # check exist email
                account = Accounts.objects(email=body['email'])

                if account:
                    return {'message': 'Duplicate account email', 'status': 'error'}, 201

                account = Accounts(**body).save()
                id = account.id
                return {'status': 'success', 'message': 'Created account successful'}, 200
            except (FieldDoesNotExist, ValidationError):
                raise SchemaValidationError
            except NotUniqueError:
                raise AccountAlreadyExistsError
            except Exception:
                raise InternalServerError
        else:
            raise UserAuthorizedError


class AccountView(MethodView):
    @jwt_required
    def put(self, id):
        user = get_jwt_identity()
        if user['role'] == 1:
            try:
                id = int(id)
                body = request.get_json()

                # required Input
                if body['balance'] is None:
                    return {'message': 'Balance do not empty!', 'status': 'error'}, 201
                if not body['firstname']:
                    return {'message': 'Firstname do not empty!', 'status': 'error'}, 201
                if not body['lastname']:
                    return {'message': 'Lastname do not empty!', 'status': 'error'}, 201
                if body['age'] is None:
                    return {'message': 'Age do not empty!', 'status': 'error'}, 201
                if body['age'] == 0:
                    return {'message': 'Age > 0!', 'status': 'error'}, 201
                if not body['gender']:
                    return {'message': 'Gender do not empty!', 'status': 'error'}, 201
                if not body['address']:
                    return {'message': 'Address do not empty!', 'status': 'error'}, 201
                if not body['email']:
                    return {'message': 'Email do not empty!', 'status': 'error'}, 201
                if not body['city']:
                    return {'message': 'City do not empty!', 'status': 'error'}, 201
                if not body['state']:
                    return {'message': 'State do not empty!', 'status': 'error'}, 201
                if not body['employer']:
                    return {'message': 'Employer do not empty!', 'status': 'error'}, 201

                # check format email
                if not (re.search(regex, body['email'])):
                    return {'message': 'Invalid Email!', 'status': 'error'}, 201

                # check exist email
                account2 = Accounts.objects(email=body['email'])

                if account2 and account2[0].account_number != id:
                    return {'message': 'Duplicate email', 'status': 'error'}, 201

                Accounts.objects(account_number=id).update(**body)
                return {'status': 'success', 'message': 'Update successful'}, 200
            except InvalidQueryError:
                raise SchemaValidationError
            except DoesNotExist:
                raise UpdatingAccountError
            except NotUniqueError:
                raise AccountAlreadyExistsError
            except Exception:
                raise InternalServerError
        else:
            raise UserAuthorizedError

    @jwt_required
    def delete(self, id):
        user = get_jwt_identity()

        if user['role'] == 1:
            try:
                id = int(id)
                Accounts.objects.get(account_number=id).delete()
                return {'status': 'success', 'message': 'Deleted successful'}, 200
            except DoesNotExist:
                raise AccountNotExistsError
            except Exception:
                raise InternalServerError
        else:
            raise UserAuthorizedError

    @jwt_required
    def get(self, id):
        user = get_jwt_identity()

        if user['role'] == 1:
            try:
                id = int(id)
                account = Accounts.objects.get_or_404(account_number=id)
                return {'status': 'success', 'data': account.to_json()}, 200
            except DoesNotExist:
                raise AccountNotExistsError
            except Exception:
                raise InternalServerError
        else:
            raise UserAuthorizedError
