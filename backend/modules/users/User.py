import re
import os

from flask import request, redirect, url_for, flash
from flask.views import MethodView
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource, reqparse
from werkzeug.utils import secure_filename

from modules.users.Users import Users

from exceptions import UserAuthorizedError, InternalServerError

ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}
APP_ROOT = os.path.dirname(os.path.abspath(__file__))

def allowed_file(filename):
  return '.' in filename and \
    filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

class UploadAvatarUserView(MethodView):
    @jwt_required
    def put(self, id):
      print('tuyen', request)
      file = request.files['formData']
      target = os.path.join(APP_ROOT, '../../static')
      if not os.path.isdir(target):
        os.mkdir(target)
      else:
        print("Couldn't create upload directory: {}".format(target))

      file = request.files['formData']
      # if user does not select file, browser also
      # submit an empty part without filename
      if file.filename == '':
          flash('No selected file')
          return redirect(request.url)
      if file and allowed_file(file.filename):
          filename = secure_filename(file.filename)
          file.save("/".join([target, filename]))
          users = Users.objects(id=id)
          if users:
            Users.objects(id=id).update(avatarUrl=url_for('static', filename=filename))
            return {'status': 'success', 'avatarUrl': url_for('static', filename=filename), 'message': 'Update avatar successful'}, 200
          else:
            return {'status': 'error', 'message': 'No user found'}, 400

class ListUserView(MethodView):
  @jwt_required
  def search(self):
    user = get_jwt_identity()
    if user['role'] == 1:
      try:
        users = Users.objects()
        total = users.count()
        return {'data': {'users': users.to_json(), 'total': total}, 'status': 'success'}, 200
      except Exception:
        raise InternalServerError
    else:
      raise UserAuthorizedError
