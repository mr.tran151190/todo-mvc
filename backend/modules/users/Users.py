# ~anm-viettel-backend/modules/users/Users.py
from extensions import db
from modules.BaseModel import BaseModel


class Users(BaseModel):
    firstname = db.StringField(required=True)
    avatarUrl = db.StringField(default='')
    lastname = db.StringField(required=True)
    email = db.EmailField(required=True, unique=True)
    password = db.StringField(required=True, min_length=6)
    role = db.IntField(required=True)
