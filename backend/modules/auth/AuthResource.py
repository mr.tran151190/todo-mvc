# ~anm-viettel-backend/modules/auth/AuthResource.py
import datetime

from flask import request, url_for
from flask.views import MethodView
from flask_jwt_extended import create_access_token
from flask_restful import Resource
from mongoengine.errors import FieldDoesNotExist, NotUniqueError, DoesNotExist, ValidationError
from werkzeug.security import generate_password_hash, check_password_hash

from exceptions import SchemaValidationError, EmailAlreadyExistsError, UnauthorizedError, InternalServerError
from modules.users.Users import Users


class RegisterView(MethodView):
    @staticmethod
    def post():
        try:
            body = request.get_json()
            user = Users(**body)
            users = Users.objects()
            user.password = generate_password_hash(request.get_json()['password'])
            user['avatarUrl'] = url_for('static', filename='user.jpg')
            if users.count() == 0:
              user['role'] = 1
            else:
              user['role'] = 2
            user.save()
            id = user.id
            return {'id': str(id)}, 200
        except FieldDoesNotExist:
            raise FieldDoesNotExist
        except ValidationError:
            raise SchemaValidationError
        except NotUniqueError:
            raise EmailAlreadyExistsError
        except Exception as e:
            raise e


class LoginView(MethodView):
    @staticmethod
    def post():
        try:
            body = request.get_json()
            user = Users.objects.get(email=body.get('email'))
            authorized = check_password_hash(
                user['password'], body['password'])
            if not authorized:
                return {'error': 'Email or password invalid'}, 400

            expires = datetime.timedelta(days=7)
            access_token = create_access_token(
                identity=({
                    'firstname': user['firstname'],
                    'lastname': user['lastname'],
                    'email': user['email'],
                    'role': user['role'],
                    'avatarUrl': user['avatarUrl']
                }), expires_delta=expires),

            print(user.to_json())

            return {'access_token': access_token, 'user': user}, 200
        except (UnauthorizedError, DoesNotExist):
            raise UnauthorizedError
        except Exception:
            raise InternalServerError

