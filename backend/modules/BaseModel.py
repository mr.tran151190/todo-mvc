from extensions import db


class BaseModel(db.Document):
    meta = {'abstract': True, 'allow_inheritance': True}

    def toJSON(self):
        response = {}
        for key in self.__table__.columns.keys():
            response[key] = getattr(self, key)

        return response
