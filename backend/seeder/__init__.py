import json

from modules.accounts.AccountModel import Accounts


class Seeder:

    def __init__(self, *args, **kwargs):
        self.app = kwargs.get('app', None)
        self.db = kwargs.get('db', None)
        self.file = kwargs.get('file', None)

    def init_app(self, *args, **kwargs):
        self.app = kwargs.get('app', None)
        self.db = kwargs.get('db', None)
        self.file = kwargs.get('file', None)

    def run(self):
        accounts_file = open(self.file, 'r')
        lines = accounts_file.readlines()

        print('Importing from file: {}'.format(self.file))

        count = 0
        for line in lines:
            acc = json.loads(line)
            account = Accounts(**acc)
            account.save()
            count += 1

        print('{} records were saved.'.format(count))
