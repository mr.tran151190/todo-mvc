from flask_script import Command, Option

from seeder import Seeder


class Seed(Command):
    option_list = (
        Option('--file', '-f', dest='file'),
    )

    def __init__(self, *args, **kwargs):
        self.app = kwargs.get('app', None)
        self.db = kwargs.get('db', None)

    def run(self, file):
        seeder = Seeder(app=self.app, db=self.db, file=file)
        seeder.run()
